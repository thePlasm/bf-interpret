#ifndef __BF_TEXT_H

#include <stdio.h>
#include <stdlib.h>

#define CHUNK_SIZE 4

size_t my_fgets(char *, size_t, FILE *);

char *get_text(FILE *);

#endif
