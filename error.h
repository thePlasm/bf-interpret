#ifndef __BF_ERROR_H

#include <stdio.h>

void file_open_error(char *);

void file_close_error(char *);

#endif
