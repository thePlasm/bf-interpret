# bf-interpret
## description
A basic brainfuck interpreter with doubly expandable memory. (Linked lists)
## how to use
Run `make' to compile everything.

Run the `bf-interpret' program.

To clean everything up, run `make clean'.
## files
The `interpreter.c' and `interpreter.h' files contain the actual BF interpreter.

The `main.c' and `main.h' files is a program that demonstrates how to use the interpreter.

The `error.c' and `error.h' files are used to print informative error messages.

The `text.c' and `text.h' files are used to handle text files.
