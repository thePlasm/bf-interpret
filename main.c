#include <stdio.h>
#include <stdlib.h>
#include "interpreter.h"
#include "text.h"
#include "error.h"

int
main(argc, argv)
int argc;
char **argv;
{
	FILE *in_file;
	FILE *out_file;
	FILE *code_file;
	char *instructions;
	int interpret_stat;
	if (argc != 4)
	{
		fprintf(stderr,
			"Usage: %s <bf code file> <input file> <output file>\n",
			argv[0]);
		return EXIT_FAILURE;
	}
	code_file = fopen(argv[1], "r");
	if (code_file == NULL)
	{
		file_open_error(argv[1]);
		return EXIT_FAILURE;
	}
	in_file = fopen(argv[2], "r");
	if (in_file == NULL)
	{
		int close_stat;
		file_open_error(argv[2]);
		close_stat = fclose(code_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[1]);
		}
		return EXIT_FAILURE;
	}
	out_file = fopen(argv[3], "w");
	if (out_file == NULL)
	{
		int close_stat;
		file_open_error(argv[3]);
		close_stat = fclose(code_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[1]);
		}
		close_stat = fclose(in_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[2]);
		}
		return EXIT_FAILURE;
	}
	instructions = get_text(code_file);
	if (instructions == NULL)
	{
		int close_stat;
		fprintf(stderr, "Error reading file %s\n", argv[1]);
		close_stat = fclose(code_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[1]);
		}
		close_stat = fclose(in_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[2]);
		}
		close_stat = fclose(out_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[3]);
		}
		return EXIT_FAILURE;
	}
	interpret_stat = interpret(instructions, in_file, out_file);
	if (interpret_stat == BF_ERR)
	{
		int close_stat;
		fprintf(stderr, "Error interpreting code\n");
		free(instructions);
		close_stat = fclose(code_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[1]);
		}
		close_stat = fclose(in_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[2]);
		}
		close_stat = fclose(out_file);
		if (close_stat == EOF)
		{
			file_close_error(argv[3]);
		}
		return EXIT_FAILURE;

	}
	return EXIT_SUCCESS;
}
