CC = gcc
CFLAGS = -Wall -Wpedantic
SRCFILES = $(wildcard *.c)
OBJFILES = $(patsubst %.c,%.o,$(SRCFILES))

.PHONY: all clean

all: bf-interpret

bf-interpret: $(OBJFILES)
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm *.o
	rm bf-interpret
