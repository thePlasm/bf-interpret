#include "text.h"

struct char_node
{
	char character[CHUNK_SIZE];
	struct char_node *next;
};

struct char_list
{
	size_t n;
	struct char_node *head;
};

size_t my_fgets(s, size, stream)
char *s;
size_t size;
FILE *stream;
{
	size_t i;
	for (i = 0; i < size; ++i)
	{
		int c = getc(stream);
		if (c == EOF)
		{
			s[i] = '\0';
			break;
		}
		s[i] = c;
	}
	return i;
}

static void
free_char_nodes(node)
struct char_node *node;
{
	while (node != NULL)
	{
		free(node);
		node = node->next;
	}
}

static struct char_list
*get_text_list(file)
FILE *file;
{
	struct char_list *list = malloc(sizeof(struct char_list));
	size_t fgets_stat;
	struct char_node *tail;
	if (list == NULL)
	{
		fprintf(stderr, "Error allocating memory for text list\n");
		return NULL;
	}
	list->n = 0;
	if (feof(file))
	{
		list->head = NULL;
		return list;
	}
	tail = list->head = malloc(sizeof(struct char_node));
	if (tail == NULL)
	{
		free(list);
		fprintf(stderr, "Error allocating memory for text list node\n");
		return NULL;
	}
	fgets_stat = my_fgets(tail->character, CHUNK_SIZE, file);
	list->n += fgets_stat;
	while (fgets_stat == CHUNK_SIZE)
	{
		tail->next = malloc(sizeof(struct char_node));
		if (tail->next == NULL)
		{
			free_char_nodes(list->head);
			free(list);
			fprintf(stderr,
				"Error allocating memory for text list node\n");
			return NULL;
		}
		tail = tail->next;
		fgets_stat = my_fgets(tail->character, CHUNK_SIZE, file);
		list->n += fgets_stat;
	}
	tail->next = NULL;
	return list;
}

char
*get_text(file)
FILE *file;
{
	struct char_list *list = get_text_list(file);
	char *text;
	size_t i;
	size_t j = 0;
	size_t n;
	struct char_node *head;
	if (list == NULL)
	{
		fprintf(stderr, "Error reading text to list\n");
		return NULL;
	}
	n = list->n;
	head = list->head;
	free(list);
	text = malloc(n + 1);
	if (text == NULL)
	{
		fprintf(stderr, "Error allocating memory for text buffer\n");
		return NULL;
	}
	for (i = 0; i < n; ++i)
	{
		if (j > 3)
		{
			struct char_node *tmp_head = head;
			head = head->next;
			free(tmp_head);
			j = 0;
		}
		if (head->character[j] == '\0')
		{
			break;
		}
		text[i] = head->character[j++];
	}
	free(head);
	text[n] = '\0';
	return text;
}

