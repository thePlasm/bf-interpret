#include "error.h"

void
file_open_error(char *fn)
{
	fprintf(stderr, "Error opening file %s: ", fn);
	perror(NULL);
}

void
file_close_error(char *fn)
{
	fprintf(stderr, "Error closing file %s: ", fn);
	perror(NULL);
}

