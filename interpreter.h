#ifndef __BF_INTERPRETER_H

#define __BF_INTERPRETER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INITIAL_MEMORY 30000
#define BF_ERR 1
#define BF_OK 0

int interpret(char *, FILE *, FILE *);

#endif
